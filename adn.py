#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
    documentation here
functions"""

__author__ = 'Marina GOLIASSE'


adn = input("Entrez une séquence d'ADN : ")
def is_valid(adn):
    str = "ATGC"
    for nucleotide in adn :
        if nucleotide not in str :
            return "FALSE"
    else :
        return "TRUE"

print(is_valid(adn))



def get_valid(adn) :
    while is_valid(adn) == "FALSE":
        adn = input("Choisissez une autre séquence :")
    print("Bonne séquence")
    print("Votre séquence finale est", adn)
